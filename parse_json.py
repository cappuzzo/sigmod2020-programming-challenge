import datetime
import pandas as pd
import os

root_dir_path = './2013_camera_specs/'
df_dir = './ds/'

for idx, dir1 in enumerate(sorted(os.listdir(root_dir_path))):
    t_start = datetime.datetime.now()
    dirp = root_dir_path+'www.garricks.com.au'
    # dirp = root_dir_path+dir1
    print(dirp)
    files = os.listdir(dirp)
    # files = os.listdir(root_dir_path + dir1)
    df = pd.DataFrame()
    for f in files:
        s = pd.read_json(dirp+'/'+f,orient='records', typ='series', dtype=False)
        s['json_index'] = int(f.split('.')[0])
        df = df.append(s, ignore_index=True)
    df = df.set_index('json_index').sort_index()
    # df.to_csv(df_dir + '{}.csv'.format(dir1), index=True)
    df.to_csv(df_dir + '{}.csv'.format('www.garricks.com.au'), index=True)
    t_end = datetime.datetime.now()
    diff = t_end - t_start
    print('Done in {} s.'.format(diff.total_seconds()))
