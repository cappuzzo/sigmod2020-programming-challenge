filename = 'sigmod_large_labelled_dataset.csv'
fout = 'positive_matches.csv'

with open(filename) as fp:
    with open(fout, 'w') as fo:
        for idx, line in enumerate(fp):
            if idx > 0:
                i1,i2,label = line.strip().split(',')
                if int(label) == 1:
                    fo.write(line)
matches = []
found = set()

with open(fout) as fp:
    for i, line in enumerate(fp):
        i1, i2, _ = line.strip().split(',')
        if i1 in found:
            for m in matches:
                if i1 in m:
                    m.add(i2)
        elif i2 in found:
            for m in matches:
                if i2 in m:
                    m.add(i1)
        else:
            matches.append({i1,i2})
            found.add(i1)
            found.add(i2)

dataset1 = 'www.shopbot.com.au'
dataset2 = 'www.pricedekho.com'

with open('{}-{}.csv'.format(dataset1, dataset2), 'w') as fp:
    for m in matches:
        for item in m:
            site, code = item.split('//')
            if site in [dataset1, dataset2]:
                fp.write('{} '.format(item))
        fp.write('\n')
